const express = require('express') //importando librerias
const bodyParser = require('body-parser')
const { request, response } = require('express')
const functions = require('./functions')
    
//instancia
const app = express()


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.get('/', function(request,response){
    response.send('Hola Mundo desde API REST')
})


app.get('/:num1/:num2',(request,response) =>{
    response.json({result: functions.suma(request.params.num1,request.params.num2)})        //result es el nombre del atributo
})// ---->/5/3

app.post('/', (request,response)=>{
    response.json({result: functions.multiply(request.body.num1,request.body.num2)})
})

//levanta la app
app.listen(3001, function(){
    console.log('Server is running in port 3001')
})
