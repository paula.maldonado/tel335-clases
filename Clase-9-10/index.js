let user

console.log('Antes')
setTimeout(() => {
    console.log('Buscar informacion...')
    user = searchUserBy(1)
    console.log('Usuario: '+ user.name)
    searchBooksByUserName(user.name, (data)=>{
        console.log(data)
    })
}, 2000)

console.log('Despues')

//Callback Hell
//Existen muchas funciones y dependen de la respuestas de ellas.



//Funciones
function searchUserBy(id){
    const promise = new Promise((resonve, reject) =>{
        setTimeout(() => {
            resolve({id, name:'Paula Maldonado'})
            
        }, 2000);
    })
    return promise
}

function searchBooksByUserName(name){
    const promise =  new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Encontrado...")
            resolve([
                {
                    id: 12,
                    book: 'Harry Potter'
                },
                {
                    id: 123,
                    book: 'La Conspiración'
                }
    
            ])
        },200)
    })

}