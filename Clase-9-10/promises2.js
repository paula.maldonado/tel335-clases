searchUserBy(1)
    .then(user =>{
        console.log('Usuario: '+ user.name)
        searchBooksByUserName(user.searchBooksByUserName)
    })
    .then(booksArray => console.log(booksArray))
    .catch(error => console.log(error))

//Callback Hell
//Existen muchas funciones y dependen de la respuestas de ellas.



//Funciones
function searchUserBy(id){
    const promise = new Promise((resolve, reject) =>{
        setTimeout(() => {
            resolve({id, name:'Paula Maldonado'})
            
        }, 2000);
    })
    return promise
}

function searchBooksByUserName(name){
    const promise =  new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Encontrado...")
            resolve([
                {
                    id: 12,
                    book: 'Harry Potter'
                },
                {
                    id: 123,
                    book: 'La Conspiración'
                }
    
            ])
        },200)
    })

}